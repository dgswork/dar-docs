# REQUISITES:
#pelican: documentacion

# Use the official image as a parent image.
FROM ubuntu:20.04

RUN apt-get update

ENV TZ=Europe/Madrid

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install python3, pelican and git
RUN apt-get -y -qq install python-is-python3 python-pelican git

# clone pelican-theme as 'clean-blog' in specified dir
RUN git clone https://github.com/gilsondev/pelican-clean-blog /usr/lib/python3/dist-packages/pelican/themes/clean-blog

# Set the working directory.
WORKDIR /mnt

# Add metadata to the image to describe which port the container is listening on at runtime.
# EXPOSE 8080

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .

# start doc server
RUN ./develop_server.sh start 8081