#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = u'DGS Team'
SITENAME = u'DarGUI Documentation'
SITEURL = 'https://www.sxgio.net/dar-gui/'
SITETITLE = AUTHOR
SITESUBTITLE = 'Graphic Interface to handle dar command'
SITEDESCRIPTION = '%s\'s Instruction Manual' % AUTHOR
ROWSER_COLOR = '#333333'
ROBOTS = 'index, follow'
PYGMENTS_STYLE = 'monokai'
PATH = 'content'
TIMEZONE = 'Europe/Madrid'
DEFAULT_LANG = u'en'
DEFAULT_PAGINATION = 10

FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blue Penguin defaults to true
DISPLAY_HEADER = True
DISPLAY_FOOTER = True
DISPLAY_HOME = False
DISPLAY_MENU = True

# General pages
TAGS_URL = '/tags.html'
AUTHORS_URL = '/authors.html'
CATEGORIES_URL = '/categories.html'
ARCHIVES_URL = '/archives.html'
ABOUT_URL = '/pages/about-here.html'
REAL_URL = 'https://www.sxgio.net/dar-gui/'
BITBUCKET_URL = 'https://bitbucket.org/dgs/'

# Additional menu items
MENUITEMS = (
    ('Home', REAL_URL),
    ('About here', ABOUT_URL),
    ('Archives', ARCHIVES_URL),
    ('Bitbucket', BITBUCKET_URL),
    ('Categories', CATEGORIES_URL),
    ('Tags', TAGS_URL),
    # ('Authors', AUTHORS_URL),
)

MAIN_MENU = True

# Theme configuration
THEME = 'clean-blog'
COLOR_SCHEME_CSS = 'monokai.css'
RELATIVE_URLS = True
SUMMARY_MAX_LENGTH = 1000
STATIC_PATHS = ['images', 'pdfs']
HEADER_COVER = '/images/main-category.jpg'
HEADER_COLOR = 'black'
DISQUS_SITENAME = 'sxgio-net'
SOCIAL = (('twitter', 'https://twitter.com/dgs-dargui'),
          ('bitbucket', 'https://bitbucket.org/dgswork/dar-docs/'),
          ('envelope', 'mailto:webmaster@sxgio.net'))
SHOW_SOCIAL_ON_INDEX_PAGE_HEADER = True
# Footer configuration
FOOTER_INCLUDE = 'dgs-footer.html'
IGNORE_FILES = [FOOTER_INCLUDE]
EXTRA_TEMPLATES_PATHS = [os.path.dirname(__file__)]
