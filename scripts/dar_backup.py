import argparse
import subprocess

parser = argparse.ArgumentParser('Arguments for dar_backup.sh')
parser.add_argument('--hostname', help="Must provide HOSTNAME", default="HOSTNAME")
parser.add_argument('--mailaddr', help="Must provide MAILADDR", default="MAILADDR")
parser.add_argument('base_service', help="Must provide BASE_SERVICE (folder path)")
parser.add_argument('--log-file', help="Must provide BASE_SERVICE (folder path)", default="/tmp/dar_backup_log")

args = parser.parse_args()

subprocess.call(['./dar_backup.sh', args.hostname, args.mailaddr])
