#!/bin/bash

# Script Name: dar_backups.sh
# Author: Sergio Martin (XtrnalLinks S.L.)
# Description: dar_backups.sh is a script to be runned from cron which 
#       backups data and stores it locally and optionally remote using scp.
#       It decides between doing a master or an incremental backup based
#       on the existance or not of a master one for the actual month.
# Source: Original Source from Roi Rodriguez Mendez & Mauro Silvosa Rivera (Cluster Digital S.L.)
# Version: 1.0
# Revision History: 
#       22.08.2005 - Creation (Original Source)
#	23.05.2006 - Modification (XtrnalLinks S.L.)

# Hostname, only for reporting
HOSTNAME=$1

# Maildir, to send information
MAILADDR=$2

# Report file
LOG_FILE=$3

# Base directory where backups are stored
BASE_DEVICE=$4

echo "Backup copy by the time "`date -I`" on $HOSTNAME " > $LOG_FILE
echo "Destination folder: $BASE_DEVICE" >> $LOG_FILE

# Base folder for backups
BASE_BAK_DIR=$BASE_DEVICE"/dar-backup"

# Directory where the backups for the current month are stored (path relative to $BASE_BAK_DIR)
MONTHLY_BAK_DIR=`date -I | awk -F "-" '{ print $1"-"$2 }'`

# Date for the current month
CURRENT_MONTH=$MONTHLY_BAK_DIR

# Name and path for the backup file.
SLICE_NAME=${BASE_BAK_DIR}/${MONTHLY_BAK_DIR}/backup_`date -I`

# Max backup file size
SLICE_SIZE=4200M

# Remote backup settings
REMOTE_BAK="false"
REMOTE_HOST="REMOTE-HOST"
REMOTE_MODULE="general"
REMOTE_USR="define-it"
REMOTE_BASE_DIR="define-it"
REMOTE_MONTHLY_DIR=$MONTHLY_BAK_DIR
REMOTE_DIR=${REMOTE_BASE_DIR}/${REMOTE_MONTHLY_DIR}

## FUNCTIONS DEFINITION

# Function which sends a mail to inform how the backup ended.
function reporting()
{
   echo "Backup copy taken place on "`date -I`" on $HOSTNAME:" >> $LOG_FILE
   echo >> $LOG_FILE
   echo "################################################">>$LOG_FILE
   echo `ls -l ${BASE_BAK_DIR}/${MONTHLY_BAK_DIR}` >> $LOG_FILE
   echo "Current system limits " >> $LOG_FILE
   du -s ${BASE_BAK_DIR}/${MONTHLY_BAK_DIR} >> $LOG_FILE
   echo "nail -s $HOSTNAME : Backup Report $MAILADDR < $LOG_FILE"
   nail -s "Backup Report" $MAILADDR < $LOG_FILE
   rm $LOG_FILE
}

# Function which creates a master backup. It gets "true" as a parameter
# if the monthly directory has to be created.
function master_bak () 
{
    if [ "$1" == "true" ]
    then
	mkdir -p ${BASE_BAK_DIR}/${MONTHLY_BAK_DIR}
    fi
    	/usr/bin/dar -m 150 -s $SLICE_SIZE -zlzo:6 -R / \
	-g ./path-to-include-1 \
        -g ./path-to-include-2 \
	-P ./path-to-exclude-1 \
	-P ./path-to-exclude-2 \
	-c ${SLICE_NAME}_master > /dev/null 
    
    if [ "$REMOTE_BAK" == "true" ]
    then
	for i in `ls ${SLICE_NAME}_master*.dar`
	do
	  /usr/bin/rsync -av --stats --timeout=3600 $i ${REMOTE_HOST}::${REMOTE_MODULE}/${REMOTE_DIR}/`basename $i` > /dev/null 
	done	
    fi
    #reporting
}

# Makes the incremental backups
function diff_bak () 
{
    MASTER=$1
    /usr/bin/dar -m 150 -s $SLICE_SIZE -zlzo:6 -R / \
    -g ./path-to-include-1 \
    -g ./path-to-include-2 \
    -P ./path-to-exclude-1 \
    -P ./path-to-exclude-2 \
    -c ${SLICE_NAME}_diff -A $MASTER > /dev/null

    if [ "$REMOTE_BAK" == "true" ]
    then
	for i in `ls ${SLICE_NAME}_diff*.dar`
	do
	  /usr/bin/rsync -av --stats --timeout=3600 $i ${REMOTE_HOST}::${REMOTE_MODULE}/${REMOTE_DIR}/`basename $i` > /dev/null 
	done
    fi
    #reporting
}

## MAIN FLUX
# Set appropriate umask value
umask 027
# Check for existing monthly backups directory
if [ ! -d ${BASE_BAK_DIR}/${MONTHLY_BAK_DIR} ]
then
    # If not, tell master_bak() to mkdir it
    master_bak "true"
else
    # Else:
    # MASTER not void if a master backup exists
    MASTER=`ls ${BASE_BAK_DIR}/${MONTHLY_BAK_DIR}/*_master*.dar | tail -n 1 | awk -F "." '{ print $1 }'`
    # Check if a master backup already exists.
    if [ "${MASTER}" != "" ]
    then
	# If it exists, a delta backup has to be done
	diff_bak $MASTER
    else
	# Otherwise , do the master backup
	master_bak "false"
    fi
fi
